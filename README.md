# Overly Complicated Calculator
**Where `0 + 0 = 90.90637642651772` and `20 - 0 = 38.87690493389959`**

## Project Description
I made this project for fun.

My attempt on making a calculator using machine learning models. I'm not sure if theres any practical use for this project since we can already make a calculator/computer that is as small as a watch that can do basic arithmetics with 100% accuracy as well as some other complicated maths.

## But Why?
1. I was bored one day
1. Got nothing to do
1. Wanted to see if I can teach my computer (or some random cloud VM, because im using Google Colaboratory) how to do math
